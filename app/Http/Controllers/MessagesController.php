<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\CreateMessageRequest;
use App\Message;

class MessagesController extends Controller
{
    public function show(Message $id){
        
        $message = Message::find($id);
        
        return view('messages.show', ['message' => $message]);
        
    }
    
    public function create(CreateMessageRequest $request){
        
        $user = $request->user();
        
        $message = Message::create([
            'user_id' => $user->id,
            'content' => $request->input('message'),
            'image' => 'http://lorempixel.com/600/338?'. mt_rand(0, 3)
        ]);
        
        return redirect('/messages/'.$message->id);
        
    }
    
}
